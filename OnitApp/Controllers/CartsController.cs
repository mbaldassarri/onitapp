using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OnitApp.Models;

namespace OnitApp.Controllers
{
    public class CartsController : Controller
    {
        private readonly ItemsContext _context;

        public CartsController(ItemsContext context)
        {
            
            _context = context;    
        }

        // GET: Carts
        public async Task<IActionResult> Index()
        {
            return View(await _context.Carts.ToListAsync());
        }

        // GET: Carts/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var cart = await _context.Carts
                .Include(c => c.Items)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.ID == id);

            if (cart == null)
            {
                return NotFound();
            }

            return View(cart);
        }

        // GET: Carts/Create
        public IActionResult Create()
        {
            PopulateDropDownItems();
            return View();
         
        }

      
        // POST: Carts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID, Code, ArriveDate,StockArea,LocationArea,Items")] Cart cart)
        {
            if (ModelState.IsValid)
            {
                
                _context.Add(cart);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(cart);
        }

        private void PopulateDropDownItems(object selectedItems = null)
            {
                var itemsQuery = from i in _context.Items
                            orderby i.ID
                            select i;
            ViewBag.Items = new SelectList(itemsQuery.AsNoTracking(), "ID",  "Note", selectedItems);
         
        }

        // GET: Carts/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var cart = await _context.Carts.AsNoTracking().SingleOrDefaultAsync(m => m.ID == id);
            if (cart == null)
            {
                return NotFound();
            }
            PopulateDropDownItems(cart.Items);
            return View(cart);
        }

        // POST: Carts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Code, ArriveDate,StockArea,LocationArea,Items")] Cart cart)
        {
            if (id != cart.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cart);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CartExists(cart.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(cart);
        }

        // GET: Carts/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            var cart = await _context.Carts.AsNoTracking()
                .SingleOrDefaultAsync(m => m.ID == id);
            if (cart == null)
            {
                return NotFound();
            }

            return View(cart);
        }

        // POST: Carts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cart = await _context.Carts.SingleOrDefaultAsync(m => m.ID == id);
            _context.Carts.Remove(cart);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool CartExists(int id)
        {
            return _context.Carts.Any(e => e.ID == id);
        }
    }
}
