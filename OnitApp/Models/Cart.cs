﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OnitApp.Models
{
    public class Cart
    {
        
        public int ID { get; set; }
        [Required]
        public string Code { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ArriveDate { get; set; }
        [Required]
        public string StockArea { get; set; }
        [Required]
        public string LocationArea { get; set; }
        public ICollection<Item> Items { get; set; }
    }
}
