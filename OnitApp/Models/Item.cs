using System;
using System.ComponentModel.DataAnnotations;

namespace OnitApp.Models
{
    public class Item
    {
        
        public int ID { get; set; }
        [Required]
        [StringLength(255)]
        public string Description { get; set; }
        public string Note { get; set; }
    }
}