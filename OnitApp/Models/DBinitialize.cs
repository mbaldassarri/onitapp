﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace OnitApp.Models
{
    public static class DBinitialize
    {
        public static void EnsureCreated(IServiceProvider serviceProvider)
        {
            var context = new ItemsContext(serviceProvider.GetRequiredService<DbContextOptions<ItemsContext>>());
            context.Database.EnsureCreated();
        }
        
    }
}
